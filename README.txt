Improvements were made in many areas of the AI. The heuristic evaluation was improved
by taking in to account corners and the number of available moves. Speed optimization
on board.cpp was also done in order to increase the depth of minimax. I think
my strategy will work because the heuristics were improved and optimized through
trials with many different constant values.

One idea I tried was iterative deepening. For some reason, I kept running out of memory
when I used it, and the AI ran more slowly.