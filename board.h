#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include "common.h"
using namespace std;

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;    
       
    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    Board();
    ~Board();
    Board *copy();
        
    bool isDone();
    bool hasMoves(Side side);
    bool checkMove(Move *m, Side side);
    void doMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();
    int countTaken();

    void setBoard(char data[]);
    int mobility(Side side, Side oppSide);
    int get_free_adjacent(int i, int j);
    int cornerDifference(Side side, Side oppSide);
    int cornerAdjacent(Side side, Side oppSide);
    int compute_score(Side side, Side oppSide);
    int evaluate(Side side, Side oppSide);
};

#endif
