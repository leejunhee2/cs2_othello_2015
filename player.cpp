#include "player.h"
#include <vector>
#include <unistd.h>
#include <limits>
#include <algorithm>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
	myColor = side;
	if (myColor == BLACK)
	{
		oppColor = WHITE;
	}
	else
	{
		oppColor = BLACK;
	}
    board = new Board();
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
	board->doMove(opponentsMove, oppColor);
	
	Move *bestMove;
	std::vector<Move*> possibleMoves;
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++) {
			Move *move = new Move(i, j);
			if (board->checkMove(move, myColor))
			{
				possibleMoves.push_back(move);
			}
		}
	}
	if (possibleMoves.size() == 0)
	{
		return NULL;
	}
	int max_index = 0;
	int max_value = std::numeric_limits<int>::min();
	if (testingMinimax)
	{
		for (unsigned int k = 0; k < possibleMoves.size(); k++)
		{
			Board *newBoard = board->copy();
			newBoard->doMove(possibleMoves[k], myColor);
			int depth = 2;
			int new_value = minimax(newBoard, oppColor, depth-1, std::numeric_limits<int>::min(), std::numeric_limits<int>::max());
			if(new_value > max_value)
			{
				max_index = k;
				max_value = new_value;
			}
			delete(newBoard);
		}
	}
	else
	{
		std::vector<int> possibleMoves_values;
		int depth = 7;
		int alpha = std::numeric_limits<int>::min();
		int beta = std::numeric_limits<int>::max();
		/*
		// Iterative starting from 2
		for (int iterative_depth = 2; iterative_depth < depth; iterative_depth = iterative_depth + 2)
		{
			for (unsigned int k = 0; k < possibleMoves.size(); k++)
			{
				Board *newBoard = board->copy();
				newBoard->doMove(possibleMoves[k], myColor);

				int new_value = minimax(newBoard, oppColor, iterative_depth-1, alpha, beta);
				possibleMoves_values.push_back(new_value);
				delete(newBoard);
			}
			quicksort_inplace(possibleMoves, possibleMoves_values, 0, possibleMoves.size()-1);
			possibleMoves_values.clear();
		}
		*/
		for (unsigned int k = 0; k < possibleMoves.size(); k++)
		{
			Board *newBoard = board->copy();
			newBoard->doMove(possibleMoves[k], myColor);
			int new_value = minimax(newBoard, oppColor, depth-1, alpha, beta);
			
			if(new_value > max_value)
			{
				max_index = k;
				max_value = new_value;
			}
			alpha = std::max(alpha, new_value);
			delete(newBoard);
		}
	}
	bestMove = possibleMoves[max_index];
	board->doMove(bestMove, myColor);
	return bestMove;
}

int Player::minimax(Board *temp, Side side, int depth, int alpha, int beta)
{
	bool max = (side == myColor);
	
	int mini;
	int a = alpha;
	int b = beta;
	Side oppSide;
	
	if (side == WHITE)
	{
		oppSide = BLACK;
	}
	else
	{
		oppSide = WHITE;
	}
	
	if (depth == 0)
	{
		if (testingMinimax) mini = temp->compute_score(myColor, oppColor);
		else mini = temp->evaluate(myColor, oppColor);
		alpha = mini;
		beta = mini;
	}
	else
	{
		if (max)
		{
			mini = std::numeric_limits<int>::min();
			bool empty = true;
			for (int i = 0; i < 8; i = i + 7)
			{
				for (int j = 0; j < 8; j = j + 7) {
					Move *move = new Move(i, j);
					if (temp->checkMove(move, side))
					{
						empty = false;
						Board *newBoard = temp->copy();
						newBoard->doMove(move, side);
						mini = std::max(mini, minimax(newBoard, oppSide, depth - 1, a, b));
						a = std::max(a, mini);
						delete(newBoard);
						if (b < a)
						{
							goto endloop;
						}
					}
					delete(move);
				}
			}
			for (int i = 0; i < 8; i++)
			{
				for (int j = 0; j < 8; j++) {
					if (!((i == 0 || i == 7) && (j == 0 || j == 7)))
						{
						Move *move = new Move(i, j);
						if (temp->checkMove(move, side))
						{
							empty = false;
							Board *newBoard = temp->copy();
							newBoard->doMove(move, side);
							mini = std::max(mini, minimax(newBoard, oppSide, depth - 1, a, b));
							a = std::max(a, mini);
							delete(newBoard);
							if (b < a)
							{
								goto endloop;
							}
						}
						delete(move);
					}
				}
			}
			if (empty)
			{
				mini = minimax(temp, oppSide, depth-1, a, b);
			}
		}
		else
		{
			mini = std::numeric_limits<int>::max();
			bool empty = true;
			for (int i = 0; i < 8; i = i + 7)
			{
				for (int j = 0; j < 8; j = j + 7) {
					Move *move = new Move(i, j);
					if (temp->checkMove(move, side))
					{
						empty = false;
						Board *newBoard = temp->copy();
						newBoard->doMove(move, side);
						mini = std::min(mini, (minimax(newBoard, oppSide, depth - 1, a, b)));
						b = std::min(b, mini);
						delete(newBoard);
						if (b < a)
						{
							goto endloop;
						}
					}
				}
			}
			for (int i = 0; i < 8; i++)
			{
				for (int j = 0; j < 8; j++) {
					if (!((i == 0 || i == 7) && (j == 0 || j == 7)))
					{
						Move *move = new Move(i, j);
						if (temp->checkMove(move, side))
						{
							empty = false;
							Board *newBoard = temp->copy();
							newBoard->doMove(move, side);
							mini = std::min(mini, (minimax(newBoard, oppSide, depth - 1, a, b)));
							b = std::min(b, mini);
							delete(newBoard);
							if (b < a)
							{
								goto endloop;
							}
						}
					}
				}
			}
			if (empty)
			{
				mini = minimax(temp, oppSide, depth-1, a, b);
			}
		}
	}
	endloop:
	return mini;
}


