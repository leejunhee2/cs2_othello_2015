#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    int minimax(Board *temp, Side side, int depth, int alpha, int beta);
    

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    Side myColor;
    Side oppColor;
    Board *board;
};

#endif
